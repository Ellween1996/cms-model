import json
import requests
import yaml
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep as s
from datetime import date

def first_letter_lowercase(text):
    return text[:1].lower() + text[1:]


class CmsModelParser:
    __config = None
    __website = None
    __token = None
    __selectedInstance = None

    def __init__(self):
        self.__apply_config()
        self.__loginAndProcess()
        self.__init_website()
        self.__parser_handler()

    def __apply_config(self):
        __current_directory = '/'.join(os.path.abspath(__file__).split('/')[:-1])
        with open(f'{__current_directory}/config.yaml', 'r') as config_file:
            self.__config = yaml.load(config_file, Loader=yaml.FullLoader)

    def __init_website(self):
        request_website = requests.get(url=self.__config['get_website'], headers={"Cookie": self.__token})
        websiteToJson = request_website.json()
        self.__website = websiteToJson['ResponseData']['CurrentWebsite']

    def __loginAndProcess(self):
        # TODO: Replace all sleeps with checks
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        driver = webdriver.Chrome(executable_path=r'drivers/chromedriver_linux', options=options)
        driver.get("https://manager.agilitycms.com")
        s(2.5)  # to make sure page and form loaded
        emailInput = driver.find_element_by_name("email")
        passwordInput = driver.find_element_by_name("password")
        emailInput.send_keys(self.__config['username'])
        passwordInput.send_keys(self.__config['password'])
        s(0.5)
        driver.find_element_by_name("submit").click()
        print("Logged In(?)\nGetting Instances");
        s(15)  # TODO: 15 seconds might not be enough, need to check via selenium if page was loaded
        driver.get(self.__config['instancesPage'])
        s(7)
        availableInstances = []
        instances = driver.find_elements_by_class_name("instance-row")
        for instance in instances:
            availableInstances.append(instance.find_element_by_tag_name("a").get_attribute("title"))
        print("Select instance to work with: ")
        for index, brand in enumerate(availableInstances):
            print("%i => %s" % (index, brand))
        selection = int(input("Enter number: "))
        print("Selected: " + availableInstances[selection])
        instances[selection].find_element_by_tag_name("a").click()
        s(3)
        settingsUrl = driver.current_url.replace("/home", "/settings/apikeys")
        driver.get(settingsUrl)
        s(4)
        websiteName = driver.find_element_by_id("websiteName").get_attribute("value")
        print("got instance id: %s" % websiteName)
        accessToken = []
        for cookie in driver.get_cookies():
            accessToken.append(cookie['name'] + "=" + cookie['value'])
        strToken = ";".join(accessToken)
        self.__token = strToken
        self.__website = websiteName
        self.__selectedInstance = availableInstances[selection]

    def __parser_handler(self):
        params = {'websiteName': self.__website, 'wrapResponse': True}
        content = {}
        content_items = {}
        request_definitions = requests.get(url=self.__config['url'], headers={"Cookie": self.__token},
                                           params=params)
        data = request_definitions.json()

        for item in data['ResponseData']:
            fields = {}
            request_fields = requests.get(url=self.__config['item_url'], headers={"Cookie": self.__token},
                                          params={'websiteName': self.__website, 'id': item['ContentDefinitionID']})
            request_fields = request_fields.json()
            for field in request_fields['ResponseData']['FieldSettings']:
                if 'ValueField' in field['FieldName']:
                    continue
                fields[first_letter_lowercase(field['FieldName'])] = None

            content_items[first_letter_lowercase(request_fields['ResponseData']['ReferenceName'])] = {
                'key': "key",
                'fields': fields
            }

            content['data'] = content_items

        with open(self.__selectedInstance + '-' + date.today().strftime("%b-%d-%Y") + '_cms_model.json', 'w') as file:
            file.write(json.dumps(content, indent=4))


if __name__ == '__main__':
    CmsModelParser()
